// Libs / Helpers
import _join from 'lodash/join';

// Relative Libs / Helpers
import printThis from './print.js';

// Styles
import './styles/main.scss';


function component() {
  var element = document.createElement('div');
  var button = document.createElement('button');

  element.innerHTML = _join(['Hello', 'webpack', 'you are cool'], ' ');
  element.setAttribute('id', 'test-element');
  element.classList.add('hello');

  button.innerHTML = 'CLICK ME';
  button.onclick = printThis;

  element.appendChild(button);

  return element;
}

function headline() {
  var headline = document.createElement('h1');

  headline.innerHTML = 'Webpack - Pacifico Font';

  headline.classList.add('headline');

  return headline;
}

document.body.appendChild(headline());
document.body.appendChild(component());
