- CSS -> single file instead of inline:
  https://webpack.js.org/plugins/extract-text-webpack-plugin/

- minify and GZIP

- watcher

- env build conditionals (see CSS ExtractTextPlugin)

- Inline font + dist/fonts output:
  https://survivejs.com/webpack/loading/fonts/