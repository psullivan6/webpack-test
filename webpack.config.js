// Libs / Helpers
const path = require('path');

// Relative Libs / Helpers
const { release: releasePath } = require('./webpack/paths.js');

// Rules
const rules_styles = require('./webpack/rules/styles.js');
const rules_images = require('./webpack/rules/images.js');
const rules_fonts = require('./webpack/rules/fonts.js');

// Plugins
const plugin_clean = require('./webpack/plugins/clean.js');
const plugins_styles = require('./webpack/plugins/styles.js');
const plugins_html = require('./webpack/plugins/html.js');


module.exports = {
  entry  : {
    app   : './src/index.js',
    print : './src/print.js'
  },
  output : {
    path     : path.resolve(__dirname, releasePath),
    filename : '[name].bundle.js'
  },
  module: {
    rules: [].concat(rules_styles, rules_images, rules_fonts)
  },
  plugins: [].concat(plugin_clean(__dirname), plugins_styles.all, plugins_html)
};
