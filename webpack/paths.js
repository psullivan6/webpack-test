var sourceDirectory   = 'src';
var compiledDirectory = 'dist';

module.exports = {
  source  : sourceDirectory,
  release : compiledDirectory
};
