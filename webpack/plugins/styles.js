// Libs / Helpers
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// Variables
const extractSass = new ExtractTextPlugin({
  filename: 'bundle.css',
  disable: ['staging', 'production'].indexOf(process.env.NODE_ENV) === -1
});


module.exports.extractSass = extractSass; // export for use in the styles rules script
module.exports.all = [extractSass]; // export for use in the main config plugins section
