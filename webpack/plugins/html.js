// Libs / Helpers
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Variables
const htmlPlugin = new HtmlWebpackPlugin();

module.exports = [htmlPlugin]; // export for use in the main config plugins section
