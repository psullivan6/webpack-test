// Libs / Helpers
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Relative Libs / Helpers
const { release: releasePath } = require('../paths.js');

module.exports = (root) => [
  new CleanWebpackPlugin(releasePath, { root })
]; // export for use in the main config plugins section
