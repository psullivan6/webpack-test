module.exports = [
  {
    test: /\.(png|svg|jpe?g|gif)$/i,
    use: [
      'file-loader'
    ]
  }
];
