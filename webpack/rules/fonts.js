module.exports = [
  {
    test: /\.(woff|woff2|eot|ttf|otf)$/i,
    use: [
      'file-loader'
    ]
  }
];
