// Relative Libs / Helpers
const plugins_styles = require('../plugins/styles.js');


module.exports = [
  {
    test: /\.css$/,
    use: [
      'style-loader',
      'css-loader'
    ]
  },
  {
    test: /\.scss$/,
    use: plugins_styles.extractSass.extract({
      use: [
        { loader: "css-loader" },
        { loader: "sass-loader" }
      ],
      fallback: "style-loader" // use style-loader in development
    })
  }
];
